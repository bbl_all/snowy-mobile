import request from '@/utils/request'

export default {
    // 获取ERP 产品单位表分页
    wmsProductUnitPage(data) {
        return request({
            url: '/wms/productunit/page',
            method: 'GET',
            data: data
        })
    },
    // 提交ERP 产品单位表表单 add为false时为编辑，默认为新增
    wmsProductUnitSubmitForm(data, add = true) {
        return request({
            url: '/wms/productunit/'+ (add ? 'add' : 'edit'),
            method: 'POST',
            data: data
        })
    },
    // 删除ERP 产品单位表
    wmsProductUnitDelete(data) {
        return request({
            url: '/wms/productunit/delete',
            method: 'POST',
            data: data
        })
    },
    // 获取ERP 产品单位表详情
    wmsProductUnitDetail(data) {
        return request({
            url: '/wms/productunit/detail',
            method: 'GET',
            data: data
        })
    },
}
