import request from '@/utils/request'

export default {
    // 获取ERP 产品分类分页
    wsmProductCategoryPage(data) {
        return request({
            url: '/wms/productcategory/page',
            method: 'GET',
            data: data
        })
    },
    // 提交ERP 产品分类表单 add为false时为编辑，默认为新增
    wsmProductCategorySubmitForm(data, add = true) {
        return request({
            url: '/wms/productcategory/'+ (add ? 'add' : 'edit'),
            method: 'POST',
            data: data
        })
    },
    // 删除ERP 产品分类
    wsmProductCategoryDelete(data) {
        return request({
            url: '/wms/productcategory/delete',
            method: 'POST',
            data: data
        })
    },
    // 获取ERP 产品分类详情
    wsmProductCategoryDetail(data) {
        return request({
            url: '/wms/productcategory/detail',
            method: 'GET',
            data: data
        })
    },
}
