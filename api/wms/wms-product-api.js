import request from '@/utils/request'

export default {
    // 获取ERP 产品表分页
    wmsProductPage(data) {
        return request({
            url: '/wms/product/page',
            method: 'GET',
            data: data
        })
    },
    // 提交ERP 产品表表单 add为false时为编辑，默认为新增
    wmsProductSubmitForm(data, add = true) {
        return request({
            url: '/wms/product/'+ (add ? 'add' : 'edit'),
            method: 'POST',
            data: data
        })
    },
    // 删除ERP 产品表
    wmsProductDelete(data) {
        return request({
            url: '/wms/product/delete',
            method: 'POST',
            data: data
        })
    },
    // 获取ERP 产品表详情
    wmsProductDetail(data) {
        return request({
            url: '/wms/product/detail',
            method: 'GET',
            data: data
        })
    },
}
